1. Application URL : https://app-{{ .Values.global.virtualServiceHost }}
2. ActiveMQ URL : https://amq-{{ .Values.global.virtualServiceHost }}
{{ if .Values.database.db_engine.h2.enabled }}
3. H2 URL : https://h2-{{ .Values.global.virtualServiceHost }}
    username: jfw
    password: jfw
    database : jdbc:h2:tcp://localhost:1521/jfw
{{ else if .Values.database.db_engine.oracle.enabled }}
3. Connect to Oracle : 
   export POD_NAME=$(kubectl get pods -n {{ .Release.Namespace }} | grep {{ .Release.Name }}-oracle | cut -d" " -f1)
   kubectl port-forward -n {{ .Release.Namespace }} $POD_NAME 1521
   username : jfw
   password : jfw
   instance : xe
{{ else if .Values.database.db_engine.mssql.enabled }}
3. Connect to MSSQL : 
   export POD_NAME=$(kubectl get pods -n {{ .Release.Namespace }} | grep {{ .Release.Name }}-mssql | cut -d" " -f1)
   kubectl port-forward -n {{ .Release.Namespace }} $POD_NAME 1433
   username : sa
   pasword : P@ssw0rd
   schema : jfw
{{ end }}