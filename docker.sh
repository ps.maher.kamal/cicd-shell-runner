#!/bin/bash
set -e

docker login -u $PSCI_HARBOR_USER -p $PSCI_HARBOR_PASS $PSCI_HARBOR_URL

rm -rf $CICD_PATH/docker/ROOT
echo "..........copying: "$WAR_PATH
export WAR_PATH="$(ls  */target/*.war |  tail -n1 | rev | cut -c5- | rev )"
cp -r $WAR_PATH/  $CICD_PATH/docker/ROOT

cd $CICD_PATH/docker
if [ ! -f ../../app-initialize.sh ]; then
    echo "app-initialize.sh not found, Use default app-initialize.sh"
    touch app-initialize.sh
    chmod +x app-initialize.sh
else
    echo "app-initialize.sh found, Use project app-initialize.sh"
    cp ../../app-initialize.sh app-initialize.sh
    chmod +x app-initialize.sh
fi

if [ "$PSCI_BRANCH" == "master" ]; then
#--------------------------------------------------------
    docker build \
        -t $PSCI_IMAGE_PATH:$PSCI_TAG \
        -t $PSCI_IMAGE_PATH:latest \
        --build-arg ARG_BUILD=$PSCI_TAG \
        --build-arg ARG_ENTITIES_PATH=$ENTITIES_PATH \
        .

    docker push $PSCI_IMAGE_PATH
    docker rmi  $PSCI_IMAGE_PATH:$PSCI_TAG
    docker rmi  $PSCI_IMAGE_PATH:latest
    rm -rf ROOT
else
#--------------------------------------------------------
    docker build \
        -t $PSCI_IMAGE_PATH:$PSCI_TAG \
        --build-arg ARG_BUILD=$PSCI_TAG \
        --build-arg ARG_ENTITIES_PATH=$ENTITIES_PATH \
        .

    docker push $PSCI_IMAGE_PATH
    docker rmi  $PSCI_IMAGE_PATH:$PSCI_TAG
    rm -rf ROOT
#--------------------------------------------------------
fi
